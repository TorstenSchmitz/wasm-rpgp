use pgp::Deserializable;
use pgp::SignedPublicKey;
use pgp::SignedSecretKey;
use pgp::composed::Message;
use pgp::crypto::sym::SymmetricKeyAlgorithm;
use pgp::types::CompressionAlgorithm;
use pgp::types::PublicKeyTrait;
use std::io::Cursor;
use wasm_bindgen::prelude::*;
#[allow(unused_imports)]
use log::{error, info, log_enabled, trace, warn};

mod utils;

#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[cfg(feature = "console_error_panic_hook")]
extern crate console_error_panic_hook;

#[cfg(not(target_family = "wasm"))]
#[ctor::ctor]
fn start() {
	warn!("Not running on wasm platform");
	env_logger::init();
}

#[wasm_bindgen(start)]
#[cfg(target_family = "wasm")]
fn start() {
	use utils;
	use wasm_logger;

	utils::set_panic_hook();

	let log_conf = wasm_logger::Config::new(log::Level::Info).module_prefix("wasm_rpgp");
	wasm_logger::init(log_conf);

	trace!("wasm module initialized, log level tracing");
}

/// Checks if ascii armored message is encrypted
#[allow(clippy::must_use_candidate)]
#[wasm_bindgen]
pub fn is_encrypted(message: &str) -> bool {
	matches!(
		Message::from_armor_single(Cursor::new(message)),
		Ok((Message::Encrypted { .. }, _))
	)
}

/// Checks if data is pgp encrypted [and compressed] data
/// works for ascii armored data too.
/// # Panics
/// if decompression fails
#[allow(clippy::must_use_candidate)]
#[wasm_bindgen]
pub fn is_encrypted_data(data: &[u8]) -> bool {
	if let Ok((m, _)) = Message::from_armor_single(Cursor::new(data)) {
		match m {
			Message::Encrypted { .. } => return true,
			Message::Compressed(m) => {
				let decomp = m.decompress().expect("failed to decompress packet");
				if let Ok(Message::Encrypted { .. }) = Message::from_bytes(decomp) {
					return true;
				}
			}
			_ => (),
		}
	}

	if let Ok(m) = Message::from_bytes(data) {
		match m {
			Message::Encrypted { .. } => return true,
			Message::Compressed(m) => {
				let decomp = m.decompress().expect("failed to decompress packet");
				if let Ok(Message::Encrypted { .. }) = Message::from_bytes(decomp) {
					return true;
				}
			}
			_ => (),
		}
	}

	false
}

/// Encrypt binary file with contents
/// of byte_data.
/// # Errors
/// Compression failure
/// or no encryption key worked.
#[wasm_bindgen]
pub fn encrypt_file(
	key: &OpaquePubkey,
	byte_data: &[u8],
	file_name: &str,
) -> Result<Vec<u8>, JsError> {
	let file = pgp::composed::Message::new_literal_bytes(file_name, byte_data);

	let compressed = file.compress(CompressionAlgorithm::ZIP)?;

	if let Ok(r) = try_encrypt_file(&compressed, &key.key) {
		trace!("main key");
		return Ok(r);
	}
	if let Ok(r) = try_encrypt_file(&compressed, &key.key.primary_key) {
		trace!("primary key");
		return Ok(r);
	}

	for key in &key.key.public_subkeys {
		if let Ok(r) = try_encrypt_file(&compressed, &key) {
			trace!(
				"encrypt_file: subkey = {} worked",
				hex_str(&key.fingerprint().as_bytes())
			);
			return Ok(r);
		}
	}

	error!("None of the keys tried worked");
	Err(JsError::new("No key worked"))
}

fn try_encrypt_file(file: &Message, key: &impl PublicKeyTrait) -> pgp::errors::Result<Vec<u8>> {
	use pgp::ser::Serialize;
	let mut rng = rand::rngs::OsRng;
	if let Ok(encrypted) =
		file.encrypt_to_keys_seipdv1(&mut rng, SymmetricKeyAlgorithm::AES256, &[&key])
	{
		let ret = encrypted.to_bytes()?;
		return Ok(ret);
	}

	Err(pgp::errors::Error::MissingKey)
}

/// Encrypt string into ascii armored pgp message
/// # Errors
/// No encryption key worked to encrypt message
#[wasm_bindgen]
pub fn encrypt_message(pubkey: &OpaquePubkey, message: &str) -> Result<String, JsError> {
	// Try primary key
	if let Ok(x) = encrypt_to_keys(message, &[&pubkey.key]) {
		trace!(
			"encrypt_message: main key = {} worked",
			hex_str(&pubkey.key.fingerprint().as_bytes())
		);
		return Ok(x);
	}
	if let Ok(x) = encrypt_to_keys(message, &[&pubkey.key.primary_key]) {
		trace!(
			"encrypt_message: primary key = {} worked",
			hex_str(&pubkey.key.fingerprint().as_bytes())
		);
		return Ok(x);
	}

	// Then every subkey
	for subkey in &pubkey.key.public_subkeys {
		if let Ok(x) = encrypt_to_keys(message, &[&subkey]) {
			trace!(
				"encrypt_message: subkey {} worked",
				hex_str(&subkey.fingerprint().as_bytes())
			);
			return Ok(x);
		}
	}

	error!("Encrypting has failed");
	Err(JsError::new("no encryption capable subkey found"))
}

fn encrypt_to_keys(message: &str, keys: &[&impl PublicKeyTrait]) -> pgp::errors::Result<String> {
	use pgp::crypto::sym::SymmetricKeyAlgorithm as alg;
	const ALG: alg = alg::AES256;
	let mut rng = rand::rngs::OsRng;

	let msg: String = Message::new_literal("__none__", message)
		.encrypt_to_keys_seipdv1(&mut rng, ALG, keys)?
		.to_armored_string(None.into())?;

	Ok(msg)
}
/// Hex stringify
fn hex_str(vec: &[u8]) -> String {
	vec.iter()
		.fold(String::new(), |acc, &rest| acc + &format!("{rest:x}"))
}

#[wasm_bindgen]
/// Public Key optained from us
/// Opaque so that it will not be serialized.
pub struct OpaquePubkey {
	pub(crate) key: SignedPublicKey,
}

/// Read ascii armored public key
/// # Errors
/// failed to parse armored public key
#[allow(clippy::unused_async)]
#[wasm_bindgen]
pub async fn read_key_armored(input: &str) -> Result<OpaquePubkey, JsError> {
	let (key, _headers) = SignedPublicKey::from_string(input)?;
	let wrapped = OpaquePubkey { key };

	if log_enabled!(log::Level::Info) {
		info!(
			"sucessfully read key {}",
			hex_str(&wrapped.key.fingerprint().as_bytes())
		);
	}

	Ok(wrapped)
}

/// Read binar pgp public key
/// # Errors
/// failure to parse binary pgp packet
#[allow(clippy::unused_async)]
#[wasm_bindgen]
pub async fn read_key_binary(input: Vec<u8>) -> Result<OpaquePubkey, JsError> {
	let key = SignedPublicKey::from_bytes(&input[..])?;
	Ok(OpaquePubkey { key })
}

use serde::{Deserialize, Serialize};
use tsify::Tsify;

/// Key type used to create
/// openpgp.js like interfaces
#[derive(Tsify, Serialize, Deserialize)]
#[tsify(into_wasm_abi, from_wasm_abi)]
pub enum Key {
	#[serde(alias = "binaryKey")]
	BinaryKey(Vec<u8>),
	#[serde(alias = "armoredKey")]
	ArmoredKey(String),
}

/// The OpenPGP.js API readKey allows
/// both binary and armored inputs
/// # Errors
/// failure to parse pgp packet
#[wasm_bindgen(js_name = readKey)]
pub async fn read_key(key: Key) -> Result<OpaquePubkey, JsError> {
	match key {
		Key::BinaryKey(k) => read_key_binary(k).await,
		Key::ArmoredKey(k) => read_key_armored(&k).await,
	}
}

/// Handle to a secret key data
/// Opaque so that it will not be serialized.
#[wasm_bindgen]
#[derive(Clone, Debug)]
pub struct OpaquePrivkeys {
	pub(crate) keys: Vec<SignedSecretKey>,
}

/// read private key data without unlocking it
/// # Errors
/// failure to parse private key data
#[allow(clippy::unused_async)]
#[wasm_bindgen(js_name = readPrivateKey)]
pub async fn read_private_key(key: Key) -> Result<OpaquePrivkeys, JsError> {
	trace!("read_private_key()");
	let all = match &key {
		Key::ArmoredKey(k) => SignedSecretKey::from_string_many(k)?.0.flatten(),
		Key::BinaryKey(k) => SignedSecretKey::from_bytes_many(&k[..]).flatten(),
	};

	Ok(OpaquePrivkeys {
		keys: all.collect(),
	})
}

/// Input struct to `create_message`
#[derive(Tsify, Serialize, Deserialize)]
#[tsify(into_wasm_abi, from_wasm_abi)]
pub enum MessageData {
	#[serde(alias = "text")]
	Text(String),
	#[serde(alias = "binary")]
	Binary(Vec<u8>),
}

#[wasm_bindgen]
#[derive(Clone, Debug)]
pub struct OpaqueMessage {
	pub(crate) message: Message,
}

/// create new message
/// Text or Binary input
/// modeled after openpgp.js
/// # Errors
/// PGP error when creating packet
#[allow(clippy::unused_async)]
#[wasm_bindgen(js_name = createMessage)]
pub async fn create_message(data: MessageData) -> Result<OpaqueMessage, JsError> {
	trace!("create message()");
	match data {
		MessageData::Text(t) => Ok(OpaqueMessage {
			message: Message::from_string(&t)?.0,
		}),
		MessageData::Binary(b) => Ok(OpaqueMessage {
			message: Message::from_bytes(&b[..])?,
		}),
	}
}

/// input to `decrypt` and `decrypt_with_password` functions
#[wasm_bindgen]
#[derive(Clone, Debug)]
pub struct DecryptionData {
	pub(crate) message: OpaqueMessage,
	pub(crate) decryption_keys: OpaquePrivkeys,
}

#[wasm_bindgen]
impl DecryptionData {
	#[wasm_bindgen(constructor)]
	/// create wrapper around message and decryption keys to pass on to decrypt
	#[must_use]
	pub fn new(message: &OpaqueMessage, decryption_keys: &OpaquePrivkeys) -> Self {
		Self {
			message: message.clone(),
			decryption_keys: decryption_keys.clone(),
		}
	}
}

/// Decrypt Message+private key pair,
/// unlock private key with password returned from callback function
/// get_pw_fn()
/// # Errors
/// none of the keys unlocked with the obtained password
/// decrypt this data succesfully
#[allow(clippy::unused_async)]
#[allow(clippy::future_not_send)]
#[wasm_bindgen]
pub async fn decrypt(
	data: &DecryptionData,
	get_pw_fn: js_sys::Function,
) -> Result<String, JsError> {
	trace!("decrypt()");
	let pw = get_pw_fn
		.call0(&JsValue::null())
		.map_err(|_e| JsError::new("failed to call pw function"))?
		.as_string()
		.ok_or_else(|| JsError::new("password function must return string"))?;

	trace!("password \"{}\" recieved", pw);
	decrypt_with_password(data, pw).await
}

/// same as decrypt but with password supplied as string
/// # Errors
/// none of the keys unlocked with the supplied password
/// decrypt this data
#[allow(clippy::unused_async)]
#[wasm_bindgen]
pub async fn decrypt_with_password(data: &DecryptionData, pw: String) -> Result<String, JsError> {
	for key in &data.decryption_keys.keys {
		trace!(
			"decrypt: trying key {}",
			hex_str(&key.fingerprint().as_bytes())
		);
		let decrypted = try_decrypt(key, &data.message.message, pw.clone());
		if let Ok(s) = decrypted {
			trace!("decrypted {}, stopping", s);
			return Ok(s);
		}
	}

	Err(JsError::new("Decryption not successfull"))
}

fn try_decrypt(
	key: &SignedSecretKey,
	msg: &Message,
	pw: String,
) -> Result<std::string::String, JsError> {
	let (message, _v) = msg.decrypt(|| pw, &[&key])?;
	if let Some(bytes) = message.get_content()? {
		let string = String::from_utf8_lossy(&bytes[..]);
		return Ok(string.into_owned());
	}

	Err(JsError::new("no message inside..."))
}

#[cfg(test)]
mod tests {

	use std::io::{BufReader, Read};

	use super::*;

	#[test]
	fn encrypt_test() -> Result<(), String> {
		let armored = std::fs::read_to_string("./tests/assets/x25519.pub.asc").unwrap();
		let resolved = tokio_test::block_on(read_key_armored(&armored));
		let parsed: OpaquePubkey = resolved.map_err(|_| "failed to parse key".to_string())?;
		assert!(parsed.key.is_signing_key());

		let msg = encrypt_message(&parsed, "TESTTING").map_err(|_| "encrypt_message failed")?;

		assert!(is_encrypted(&msg), "failing message = {}", &msg);

		Ok(())
	}

	#[test]
	fn decrypt_test() -> Result<(), String> {
		let plaintext = "__TESTING__";
		let armoredpubkey = std::fs::read_to_string("./tests/assets/x25519.pub.asc").unwrap();
		let pubkey = tokio_test::block_on(read_key_armored(&armoredpubkey))
			.map_err(|_| "failed to parse key".to_string())?;

		let ciphertext =
			encrypt_message(&pubkey, plaintext).map_err(|_| "encrypt_message failed")?;

		let armoredprivkey = std::fs::read_to_string("./tests/assets/x25519.sec.asc").unwrap();
		let privkeys = tokio_test::block_on(read_private_key(Key::ArmoredKey(armoredprivkey)))
			.map_err(|_| "failed to parse key".to_string())?;

		let msg = tokio_test::block_on(create_message(MessageData::Text(ciphertext)))
			.map_err(|_| "failed to create message".to_string())?;

		let data = DecryptionData::new(&msg, &privkeys);

		let decrypted = tokio_test::block_on(decrypt_with_password(&data, "".to_string()))
			.map_err(|_| "decryption failed".to_string())?;

		assert_eq!(decrypted, plaintext);

		Ok(())
	}

	#[test]
	fn encrypt_file_test() -> Result<(), String> {
		let armored = std::fs::read_to_string("./tests/assets/x25519.pub.asc").unwrap();
		let resolved = tokio_test::block_on(read_key_armored(&armored));
		let parsed: OpaquePubkey = resolved.map_err(|_| "failed to parse key".to_string())?;
		assert!(parsed.key.is_signing_key());

		let file = std::fs::File::open("./tests/assets/lorem-ipsum.txt").unwrap();
		let filesize: usize = file.metadata().unwrap().len().try_into().unwrap();

		let mut buf_reader = BufReader::new(file);
		let mut buf: Vec<u8> = Vec::<u8>::with_capacity(filesize);
		let _read = buf_reader
			.read_to_end(&mut buf)
			.map_err(|e| e.to_string())?;

		let ciphertext =
			encrypt_file(&parsed, &buf, "lorem-ipsum.txt").map_err(|_| "encrypt file failed")?;

		assert!(is_encrypted_data(&ciphertext), "Bytes show not encrypted");

		// check it decrypts to the same thing
		let armoredprivkey = std::fs::read_to_string("./tests/assets/x25519.sec.asc").unwrap();
		let privkeys = tokio_test::block_on(read_private_key(Key::ArmoredKey(armoredprivkey)))
			.map_err(|_| "failed to parse key".to_string())?;
		let data = tokio_test::block_on(create_message(MessageData::Binary(ciphertext)))
			.map_err(|_| "failed to package message".to_string())?;
		let data = DecryptionData::new(&data, &privkeys);

		let decrypted = tokio_test::block_on(decrypt_with_password(&data, "".to_string()))
			.map_err(|_| "decryption failed".to_string())?;
		assert!(buf.eq(decrypted.as_bytes()));
		Ok(())
	}
}
