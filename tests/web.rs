//! Test suite for the Web and headless browsers.

#![cfg(target_arch = "wasm32")]

wasm_bindgen_test_configure!(run_in_browser);
use wasm_bindgen::{JsCast, JsError, JsValue};
use wasm_bindgen_test::*;

#[wasm_bindgen_test]
async fn test_read_key_runs_web() -> Result<(), JsValue> {
	let armored = r#"
-----BEGIN PGP PUBLIC KEY BLOCK-----

mDMEZYwaaRYJKwYBBAHaRw8BAQdAB1/IdgODR7Qf3I41YB2cGNpWMzF9UWi84EGt
g9Z7QzW0KEpvaG4gRG9lICh0ZXN0IGtleSBncGcpIDxqZEBleGFtcGxlLm9yZz6I
kAQTFgoAOBYhBG1nHavRtcfIUPNDf5jScL38tHuJBQJljBppAhsDBQsJCAcDBRUK
CQgLBRYDAgEAAh4FAheAAAoJEJjScL38tHuJ+osA/RLBkYCV1hj3koXEQ6WXL67+
DSPlgUxNhOlJMSLyz1EEAP4jnnb3nt1z3MWeeun1NEpE/gXlGp46sZ+FE8r1A6fv
CLg4BGWMGmkSCisGAQQBl1UBBQEBB0CegMnzqkiLwgSHcGG1hZOyRJ8bNF7suvZx
dlmDtFJAdAMBCAeIeAQYFgoAIBYhBG1nHavRtcfIUPNDf5jScL38tHuJBQJljBpp
AhsMAAoJEJjScL38tHuJpccA/17HGTXV42Wj8GTnp9SXls4jOnN7oCBl+vN1p2+y
9fKDAQDWr0kabdm18ARoDewp1wszsUeZhl8vQlts2XBW7lIhCA==
=q9nC
-----END PGP PUBLIC KEY BLOCK-----
    "#;
	let _key = wasm_rpgp::read_key_armored(&armored).await?;
	Ok(())
}

#[wasm_bindgen_test]
async fn encrypt_decrypt_test_web() -> Result<(), JsValue> {
	let armored = include_str!("./assets/x25519.pub.asc");
	let parsed = wasm_rpgp::read_key_armored(&armored)
		.await
		.map_err(|_| JsError::new("failed to parse key"))?;

	let file = include_bytes!("./assets/lorem-ipsum.txt");
	let ciphertext = wasm_rpgp::encrypt_file(&parsed, file, "lorem-ipsum.txt")
		.map_err(|_| JsError::new("encrypt file failed"))?;
	assert!(
		wasm_rpgp::is_encrypted_data(&ciphertext),
		"Bytes show not encrypted"
	);

	// check it decrypts to the same thing
	let armoredprivkey = include_str!("./assets/x25519.sec.asc").to_string();
	let privkeys = wasm_rpgp::read_private_key(wasm_rpgp::Key::ArmoredKey(armoredprivkey))
		.await
		.map_err(|_| JsError::new("failed to parse key"))?;
	let data = wasm_rpgp::create_message(wasm_rpgp::MessageData::Binary(ciphertext))
		.await
		.map_err(|_| JsError::new("failed to package message"))?;
	let data = wasm_rpgp::DecryptionData::new(&data, &privkeys);

	let f: js_sys::Function =
		wasm_bindgen::closure::Closure::once_into_js(move || "".to_string()).unchecked_into();
	let decrypted = wasm_rpgp::decrypt(&data, f)
		.await
		.map_err(|_| JsError::new("decryption failed"))?;
	assert!(file.eq(decrypted.as_bytes()));
	Ok(())
}
