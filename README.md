# Disclaimer

Don't use this!

This is my personal wasm wrapper of [rpgp](https://github.com/rpgp/rpgp/).

More serious efforts should be directed to [rpgp-js](https://github.com/rpgp/rpgp-js).

## License

This work is dual-licensed under Apache 2.0 and MIT.

`SPDX-License-Identifier: MIT OR Apache-2.0`